package kol1.app.model;


import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;





@Entity
public class PravoPristupa {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	
	int kod;
	LocalDateTime datumIsteka;
	
	
	
	
	@ManyToOne
	@JoinColumn(name = "datoteka_id")
	Datoteka datoteka;

	
	
	@OneToMany(mappedBy = "pravo",fetch = FetchType.LAZY)
	List<Korisnik> listaKorisnika;
	
	
	



	public PravoPristupa() {
		
	}






	public PravoPristupa(Long id, int kod, LocalDateTime datumIsteka, Datoteka datoteka,
			List<Korisnik> listaKorisnika) {
		super();
		this.id = id;
		this.kod = kod;
		this.datumIsteka = datumIsteka;
		this.datoteka = datoteka;
		this.listaKorisnika = listaKorisnika;
	}






	public Long getId() {
		return id;
	}






	public void setId(Long id) {
		this.id = id;
	}






	public int getKod() {
		return kod;
	}






	public void setKod(int kod) {
		this.kod = kod;
	}






	public LocalDateTime getDatumIsteka() {
		return datumIsteka;
	}






	public void setDatumIsteka(LocalDateTime datumIsteka) {
		this.datumIsteka = datumIsteka;
	}






	public Datoteka getDatoteka() {
		return datoteka;
	}






	public void setDatoteka(Datoteka datoteka) {
		this.datoteka = datoteka;
	}






	public List<Korisnik> getListaKorisnika() {
		return listaKorisnika;
	}






	public void setListaKorisnika(List<Korisnik> listaKorisnika) {
		this.listaKorisnika = listaKorisnika;
	}

	
	



	
	
	
	
	
	
	
	
	
}
