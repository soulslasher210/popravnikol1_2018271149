package kol1.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



@Entity
public class Korisnik {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	String korisnickoIme;
	String lozinka;
	
	
	@ManyToOne
	@JoinColumn(name = "pravo_id")
	PravoPristupa pravo;


	
	
	
	
	
	public Korisnik() {
	
	}
	
	


	public Korisnik(Long id, String korisnickoIme, String lozinka, PravoPristupa pravo) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.pravo = pravo;
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getKorisnickoIme() {
		return korisnickoIme;
	}


	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}


	public String getLozinka() {
		return lozinka;
	}


	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}


	public PravoPristupa getPravo() {
		return pravo;
	}


	public void setPravo(PravoPristupa pravo) {
		this.pravo = pravo;
	}
	
	
	
	
	
	
	
	
}
