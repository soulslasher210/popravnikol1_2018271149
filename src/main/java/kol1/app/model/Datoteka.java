package kol1.app.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class Datoteka {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	
	String putanja;
	LocalDateTime datumKreiranja;
	int velicina;
	
	
	@OneToMany(mappedBy = "datoteka",fetch = FetchType.LAZY)
	List<PravoPristupa> lista;

	
	
	public Datoteka() {
	
	}
	
	


	public Datoteka(Long id, String putanja, LocalDateTime datumKreiranja, int velicina, List<PravoPristupa> lista) {
		super();
		this.id = id;
		this.putanja = putanja;
		this.datumKreiranja = datumKreiranja;
		this.velicina = velicina;
		this.lista = lista;
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getPutanja() {
		return putanja;
	}


	public void setPutanja(String putanja) {
		this.putanja = putanja;
	}


	public LocalDateTime getDatumKreiranja() {
		return datumKreiranja;
	}


	public void setDatumKreiranja(LocalDateTime datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}


	public int getVelicina() {
		return velicina;
	}


	public void setVelicina(int velicina) {
		this.velicina = velicina;
	}


	public List<PravoPristupa> getLista() {
		return lista;
	}


	public void setLista(List<PravoPristupa> lista) {
		this.lista = lista;
	}
	
	
	

}
