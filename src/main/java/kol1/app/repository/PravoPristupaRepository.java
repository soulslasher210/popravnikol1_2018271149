package kol1.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import kol1.app.model.PravoPristupa;

@Repository
public interface PravoPristupaRepository extends CrudRepository<PravoPristupa, Long> {

}
