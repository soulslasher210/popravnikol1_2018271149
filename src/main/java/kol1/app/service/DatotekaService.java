package kol1.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kol1.app.model.Datoteka;
import kol1.app.repository.DatotekaRepository;


@Service
public class DatotekaService {
	@Autowired
	DatotekaRepository repository;
	
	public Iterable<Datoteka> dobaviSve(){
		return repository.findAll();
	}
	
	public Datoteka dobaviPoId(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public void save(Datoteka obj) {
		repository.save(obj);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}
}
