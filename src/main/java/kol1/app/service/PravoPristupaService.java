package kol1.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kol1.app.model.PravoPristupa;
import kol1.app.repository.PravoPristupaRepository;



@Service
public class PravoPristupaService {
	@Autowired
	PravoPristupaRepository repository;
	
	public Iterable<PravoPristupa> dobaviSve(){
		return repository.findAll();
	}
	
	public PravoPristupa dobaviPoId(Long id) {
		return repository.findById(id).orElse(null);
	}
	
	public void save(PravoPristupa obj) {
		repository.save(obj);
	}
	
	public void delete(Long id) {
		repository.deleteById(id);
	}

}
