package kol1.app.dto;

public class KorisnikDTO {

	Long id;
	String korisnickoIme;
	String lozinka;
	
	PravoPristupaDTO pravo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public PravoPristupaDTO getPravo() {
		return pravo;
	}

	public void setPravo(PravoPristupaDTO pravo) {
		this.pravo = pravo;
	}
	
	
}
