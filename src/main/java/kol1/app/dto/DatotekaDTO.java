package kol1.app.dto;

import java.time.LocalDateTime;
import java.util.List;

public class DatotekaDTO {
	
	Long id;
	String putanja;
	LocalDateTime datumKreiranja;
	int velicina;
	
	List<PravoPristupaDTO>lista;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPutanja() {
		return putanja;
	}

	public void setPutanja(String putanja) {
		this.putanja = putanja;
	}

	public LocalDateTime getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(LocalDateTime datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public int getVelicina() {
		return velicina;
	}

	public void setVelicina(int velicina) {
		this.velicina = velicina;
	}

	public List<PravoPristupaDTO> getLista() {
		return lista;
	}

	public void setLista(List<PravoPristupaDTO> lista) {
		this.lista = lista;
	}

}
