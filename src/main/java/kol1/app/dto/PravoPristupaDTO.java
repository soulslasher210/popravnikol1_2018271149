package kol1.app.dto;

import java.time.LocalDateTime;
import java.util.List;

public class PravoPristupaDTO {
	
	Long id;
	int kod;
	LocalDateTime datumIsteka;
	
	DatotekaDTO datoteka;
	List<KorisnikDTO>listaKorisnika;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getKod() {
		return kod;
	}
	public void setKod(int kod) {
		this.kod = kod;
	}
	public LocalDateTime getDatumIsteka() {
		return datumIsteka;
	}
	public void setDatumIsteka(LocalDateTime datumIsteka) {
		this.datumIsteka = datumIsteka;
	}
	public DatotekaDTO getDatoteka() {
		return datoteka;
	}
	public void setDatoteka(DatotekaDTO datoteka) {
		this.datoteka = datoteka;
	}
	public List<KorisnikDTO> getListaKorisnika() {
		return listaKorisnika;
	}
	public void setListaKorisnika(List<KorisnikDTO> listaKorisnika) {
		this.listaKorisnika = listaKorisnika;
	}
	
	
	

}
