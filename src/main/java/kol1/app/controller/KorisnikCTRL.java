package kol1.app.controller;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kol1.app.dto.KorisnikDTO;
import kol1.app.model.Korisnik;
import kol1.app.service.KorisnikService;



@Controller
@CrossOrigin
@RequestMapping(path = "/korisnik")
public class KorisnikCTRL {
	@Autowired
	KorisnikService service;
	ArrayList<KorisnikDTO> lista;
	
	
	
	//Dobavi sve
		@RequestMapping(path = "", method = RequestMethod.GET)
		public ResponseEntity<ArrayList<KorisnikDTO>> dobaviSve(){
			ModelMapper mm = new ModelMapper();
			
			lista = new ArrayList<KorisnikDTO>();
			for(Korisnik x:service.dobaviSve()) {
				lista.add(mm.map(x, KorisnikDTO.class));
			}
			
			return new ResponseEntity<ArrayList<KorisnikDTO>>(lista, HttpStatus.OK);
		}
		
		//Dobavi po ID
		@RequestMapping(path = "/{id}", method = RequestMethod.GET)
		public ResponseEntity<KorisnikDTO> dobaviPoId(@PathVariable("id") Long id){
			ModelMapper mm = new ModelMapper();
			Korisnik postojeci = service.dobaviPoId(id);
			
			if(postojeci == null) {
				return new ResponseEntity<KorisnikDTO>(HttpStatus.NOT_FOUND);
			}
			KorisnikDTO obj = mm.map(postojeci, KorisnikDTO.class);
			return new ResponseEntity<KorisnikDTO>(obj, HttpStatus.OK);
			
		}
		
		//Dodavanje novog
		@RequestMapping(path = "", method = RequestMethod.POST)
	    public ResponseEntity<Korisnik> dodajNovi(@RequestBody Korisnik obj) {
	        if (service.dobaviPoId(obj.getId()) != null) {
	            return new ResponseEntity<Korisnik>(HttpStatus.CONFLICT);
	        }
	        service.save(obj);
	        return new ResponseEntity<Korisnik>(HttpStatus.OK);
	    }
		
		//Izmena
	    @RequestMapping(path = "", method = RequestMethod.PUT)
	    public ResponseEntity<Korisnik> izmeni(@RequestBody Korisnik obj) {
	        Korisnik postojeci = service.dobaviPoId(obj.getId());
	        
	        if (postojeci == null) {
	            return new ResponseEntity<Korisnik>(HttpStatus.NOT_FOUND);
	        }
	        
	        
	      postojeci.setKorisnickoIme(obj.getKorisnickoIme());
	      postojeci.setLozinka(obj.getLozinka());
	      postojeci.setPravo(obj.getPravo());
	        
	        
	        service.save(postojeci);
	        return new ResponseEntity<Korisnik>(HttpStatus.OK);
	    }

	    //Brisanje
	    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	    public ResponseEntity<?> brisanje(@PathVariable("id") Long id) {
	    	
	        if (service.dobaviPoId(id) == null) {
	            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
	        }
	        
	        service.delete(id);
	        return new ResponseEntity<Object>(HttpStatus.OK);
	    }
	

}
