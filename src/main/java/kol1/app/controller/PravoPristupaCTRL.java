package kol1.app.controller;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kol1.app.dto.PravoPristupaDTO;
import kol1.app.model.PravoPristupa;
import kol1.app.service.PravoPristupaService;


@Controller
@CrossOrigin
@RequestMapping(path = "/pravoPristupa")
public class PravoPristupaCTRL {
	@Autowired
	PravoPristupaService service;
	ArrayList<PravoPristupaDTO> lista;
	
	//Dobavi sve
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<PravoPristupaDTO>> dobaviSve(){
		ModelMapper mm = new ModelMapper();
		
		lista = new ArrayList<PravoPristupaDTO>();
		for(PravoPristupa x:service.dobaviSve()) {
			lista.add(mm.map(x, PravoPristupaDTO.class));
		}
		
		return new ResponseEntity<ArrayList<PravoPristupaDTO>>(lista, HttpStatus.OK);
	}
	
	//Dobavi po ID
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<PravoPristupaDTO> dobaviPoId(@PathVariable("id") Long id){
		ModelMapper mm = new ModelMapper();
		PravoPristupa postojeci = service.dobaviPoId(id);
		
		if(postojeci == null) {
			return new ResponseEntity<PravoPristupaDTO>(HttpStatus.NOT_FOUND);
		}
		PravoPristupaDTO obj = mm.map(postojeci, PravoPristupaDTO.class);
		return new ResponseEntity<PravoPristupaDTO>(obj, HttpStatus.OK);
		
	}
	
	//Dodavanje novog
	@RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<PravoPristupa> dodajNovi(@RequestBody PravoPristupa obj) {
        if (service.dobaviPoId(obj.getId()) != null) {
            return new ResponseEntity<PravoPristupa>(HttpStatus.CONFLICT);
        }
        service.save(obj);
        return new ResponseEntity<PravoPristupa>(HttpStatus.OK);
    }
	
	//Izmena
    @RequestMapping(path = "", method = RequestMethod.PUT)
    public ResponseEntity<PravoPristupa> izmeni(@RequestBody PravoPristupa obj) {
        PravoPristupa postojeci = service.dobaviPoId(obj.getId());
        
        if (postojeci == null) {
            return new ResponseEntity<PravoPristupa>(HttpStatus.NOT_FOUND);
        }
        
        
        postojeci.setKod(obj.getKod());
        postojeci.setDatumIsteka(obj.getDatumIsteka());
        
        
        service.save(postojeci);
        return new ResponseEntity<PravoPristupa>(HttpStatus.OK);
    }

    //Brisanje
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> brisanje(@PathVariable("id") Long id) {
    	
        if (service.dobaviPoId(id) == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        
        service.delete(id);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
