package kol1.app.controller;

import java.util.ArrayList;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import kol1.app.dto.DatotekaDTO;
import kol1.app.model.Datoteka;
import kol1.app.service.DatotekaService;


@Controller
@CrossOrigin
@RequestMapping(path = "/datoteka")

public class DatotekaCTRL {
	@Autowired
	DatotekaService service;
	ArrayList<DatotekaDTO> lista;
	//Dobavi sve
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<DatotekaDTO>> dobaviSve(){
		ModelMapper mm = new ModelMapper();
		
		lista = new ArrayList<DatotekaDTO>();
		for(Datoteka x:service.dobaviSve()) {
			lista.add(mm.map(x, DatotekaDTO.class));
		}
		
		return new ResponseEntity<ArrayList<DatotekaDTO>>(lista, HttpStatus.OK);
	}
	
	//Dobavi po ID
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<DatotekaDTO> dobaviPoId(@PathVariable("id") Long id){
		ModelMapper mm = new ModelMapper();
		Datoteka postojeci = service.dobaviPoId(id);
		
		if(postojeci == null) {
			return new ResponseEntity<DatotekaDTO>(HttpStatus.NOT_FOUND);
		}
		DatotekaDTO obj = mm.map(postojeci, DatotekaDTO.class);
		return new ResponseEntity<DatotekaDTO>(obj, HttpStatus.OK);
		
	}
	
	//Dodavanje novog
	@RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<Datoteka> dodajNovi(@RequestBody Datoteka obj) {
        if (service.dobaviPoId(obj.getId()) != null) {
            return new ResponseEntity<Datoteka>(HttpStatus.CONFLICT);
        }
        service.save(obj);
        return new ResponseEntity<Datoteka>(HttpStatus.OK);
    }
	
	//Izmena
    @RequestMapping(path = "", method = RequestMethod.PUT)
    public ResponseEntity<Datoteka> izmeni(@RequestBody Datoteka obj) {
        Datoteka postojeci = service.dobaviPoId(obj.getId());
        
        if (postojeci == null) {
            return new ResponseEntity<Datoteka>(HttpStatus.NOT_FOUND);
        }
        
        
        postojeci.setDatumKreiranja(obj.getDatumKreiranja());
        postojeci.setPutanja(obj.getPutanja());
        postojeci.setVelicina(obj.getVelicina());
       
        
        
        service.save(postojeci);
        return new ResponseEntity<Datoteka>(HttpStatus.OK);
    }

    //Brisanje
    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> brisanje(@PathVariable("id") Long id) {
    	
        if (service.dobaviPoId(id) == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        
        service.delete(id);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

}
